## Version: [version number]

### Notable Changes:

* Item1
* Item2
* Item3

### Explanations of Changes (if Needed):



### Merge Checklist:

- [ ] Changelog entry
- [ ] Changelog entry includes upgrade path (if needed)
- [ ] Changes tested (if needed)
- [ ] Configuration file example updated (if needed)
- [ ] Copyright year entries updated (if needed)
- [ ] Dependencies updated (if needed)
- [ ] Documentation updated (if needed)
- [ ] Screenshots updated (if needed)
- [ ] Readme updated (if needed)
- [ ] Translations recompiled (if needed)
- [ ] The application version has been bumped in the [layout template](app/templates/layout.html) footer
- [ ] Unnecessary git tags for previous commits in this MR removed
      (`git tag -d vX.Y.Z`)
- [ ] Git tag created for final commit prior to merge (`git tag vX.Y.Z`)
- [ ] Git tag pushed (`git push origin vX.Y.Z`)
