# Changelog
All notable changes to this project will be documented in this file, including:

- Changes requiring editing the config file
- Changes requiring recompiling translations
- Changes requiring newer versions of Python
- Changes requiring pruning blacklist additions
- Changes requiring updating dependencies

## [1.2.0] - 2023-04-??
### Added
- Fallback address for HTTP/0.9 and 1.0 clients (#9)
- Uploads form action attribute (#9)
- Upload support for UdiWWW (#9)
- User-agent to footer (#8)
- UTF-8 disabling parameter (#7)
- Acknowledgement for evolt team for archiving so many old browsers
- GitLab MR template

### Changed
- Updated copyright year

### Upgrade Path
- Config file - `fallback_address` introduced, but it is optional (see readme
  for details)
- Recompile translations

## [1.1.1] - 2022-05-07
### Added
- Babel for i18n (#4)
- Conditions that will be noted on all future releases when additional work is
  required to upgrade the application beyond a simple git pull
- NCSA Mosaic 1.0, 2.0 for Windows to comaptibility chart (#5)
- UdiWWW 1.2 to compatibility chart (#5)

### Changed
- Extracted strings to external file for i18n (#4)
- Made decimal delimiter customizable (#4)
- Updated readme

### Removed
- Pipfile.lock from git (#6)

### Upgrade Path
- Upgrade dependencies
- Compile translations

## [1.1.0] - 2022-04-28
### Added
- Ability to display MIME type of uploaded files (#2)
- Added typing information, where possible

### Changed
- Bumped minimum Python version to 3.8
- Fixed incorrect file size calculation (1000 vs 1024) (#3)
- Updated readme

## [1.0.5] - 2022-01-01
### Added
- SystemD template unit file and instructions on how to use it

## [1.0.4] - 2021-12-31
### Changed
- Fixed env file being excluded by gitignore

## [1.0.3] - 2021-12-20
### Changed
- Bumped copyright year
- FLASK\_APP variable now automatically set
- Updated readme

## [1.0.2] - 2021-09-22
### Changed
- Minor formatting changes
- Updated readme

## [1.0.1] - 2021-02-25
### Added
- Talked about deployment options in readme

### Changed
- Copyright footer in layout template
- Minimum Python version in pipfile to fix 3.6 exclusivity (#1)

## [1.0.0] - 2020-08-02
### Added
- Footer with copyright info and a link to the source code

### Changed
- Updated readme

## [0.5.0] - 2020-08-02
### Changed
- Moved configuration variables into a separate file

## [0.4.0] - 2020-08-02
### Added
- File listing on index page

## [0.3.0] - 2020-08-02
### Added
- Error messages
- Flask render templates

## [0.2.0] - 2020-08-02
### Added
- Upload page
- gitignore file

## [0.1.0] - 2020-08-02
### Added
- Basic Flask page
- License
- Readme

