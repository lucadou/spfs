# SPFS - Simple Python File Server

A very simple Python Flask application for uploading and downloading files.
You can upload files, list out the uploaded files, and download files.
Works with Internet Explorer 4+/Netscape Navigator 2+,
as well as any modern browser.
Intended for use on local networks only, not public facing ones.

### Table of Contents

* [Setup](#setup)
  * [Requirements](#requirements)
  * [Dependency Setup](#dependency-setup)
  * [Running SPFS](#running-spfs)
  * [Updating SPFS](#upgrading-spfs)
* [Compatibility](#compatibility)
  * [Minimum Supported Browsers](#minimum-supported-browsers)
* [Notes/FAQ](#notesfaq)
* [Versioning](#versioning)
* [Contributing](#contributing)
  * [Localization](#localization)
* [Authors and Acknowledgments](#authors-and-acknowledgments)
* [License](#license)

![SPFS running in Internet Explorer 5 on Mac OS 9](docs/img/ms_ie/002-ie-5.0-mac.png "SPFS running in Internet Explorer 5 on Mac OS 9")

## Setup

### Requirements

* Python 3.8+ (uses TypedDict from 3.8; tested with 3.8 but should
  work with all later releases)
  * If you do not have 3.8+, I recommend using
    [asdf](http://asdf-vm.com/guide/getting-started.html) with the
    [Python plugin](https://github.com/danhper/asdf-python) to easily install
    newer versions
* pip3
* [file](https://linux.die.net/man/1/file) (installed by default on basically
  every Linux distro and Unix derivative)
  * Technically, you just need [libmagic](https://linux.die.net/man/3/libmagic),
    but if you have `file`, you have libmagic
  * If on Windows (non-WSL), pipenv will automatically install
    `python-magic-bin`, which contains Windows binaries (uses the
    [sys_platform feature](https://pipenv.pypa.io/en/latest/advanced/#specifying-basically-anything))

### Dependency Setup

```bash
cd app/
pip3 install --user pipenv # if not already installed
# If system Python is <3.8:
pipenv --python 3.8 # can also specify 3.9 or 3.10
# If ^ errors saying it needs pyenv or asdf, install asdf
# (see Requirements section above)
pipenv install
```

### Running SPFS

When you are editing the configuration file, you will note that the
`fallback_address` variable is optional.
This is the domain name whose DNS entries points to the server.

It is recommended that you either specify a domain name or just delete the line
entirely (do not specify an IP address).
This is for two reasons:

1. The server might not have a static IP address
2. Since you can only specify one `SERVER_NAME` for Flask, you should not
   force it to be IPv4 or IPv6-only

This variable is only used for browsers where not including the hostname in URLs
could cause problems, and those clients must be able to access the fallback
address.
In practice, these clients are HTTP/0.9 and older HTTP/1.0 implementations;
modern clients will never trigger the code paths that lead to the variable
being used.

When Flask is behind a reverse proxy like nginx, you cannot reliably tell if
IPv4 or IPv6 was used for a given request (the `Host` header usually contains
the domain name), so in the event that this variable is required, it should
be left up to the client whether to use an A or AAAA record because it knows
what it can access better than the application.

Running the application (on Linux):

```bash
cd app/
cp config.json.example config.json
# edit the config file with your editor of choice, then...
pipenv shell
pybabel compile -d translations
flask run
```

Running the application (on Windows):

```bash
cd app
copy config.json.example config.json
# edit the config file with your editor of choice, then...
pipenv shell
pybabel compile -d translations
flask run
```

### Upgrading SPFS

Normally, a simple `git pull` will suffice.

However, if any additional action is required when upgrading the application
(e.g. upgrading dependencies, recompiling translations, etc.), it will be
documented inside the changelog.

Therefore, when upgrading SPFS, you should always make sure to check the
changelog.

Here are some common scenarios:

* Dependency upgrades - `pipenv install`
* Editing the configuration file - `git diff config.json config.json.example`
  and make changes accordingly (any changes to acceptable values will be
  communicated in the changelog)
* Python version bump - this will be documented in the changelog and it will be
  up to you to perform
* Reconciling blacklists - `comm -1 -2 app/utf8_blacklist_base.json app/utf8_blacklist_additions.json`
  will show the lines in common between the upstream blacklist and your own,
  and using this you can prune your blacklist additions
  * Technically this is not required, but it is best practice to prune the
    blacklists as needed
* Translation changes - `pybabel compile -d translations`

## Compatibility

*For a detailed breakdown on browser compatibility, please see the
[COMPATIBILITY](docs/COMPATIBILITY.md) page.*

This application supports many browsers, but certainly not all of them.
Specifically, it depends on HTML 3.0, which introduced the
[type=file](https://www.w3.org/MarkUp/html3/input.html) attribute to the `input`
element.
HTML 2.0, by contrast,
[did not have this](https://datatracker.ietf.org/doc/html/rfc1866#section-8.1.2).

It also requires at least HTTP/1.0 with the capability to receive HTTP/1.1
responses; that is, this application does not use any HTTP/1.1 features, but
Flask implements the HTTP/1.1 suggestion to
[reply to 1.0 requests with 1.1 responses](https://datatracker.ietf.org/doc/html/rfc2145#section-2.3),
so the browser must ignore the HTTP version number if it doesn't support 1.1.
(Many 1.0 browsers do this, but some do not, such as Netscape 0.93 and 0.96
for Mac.)

However, if you simply wish to view and download files,
[HTTP/0.9](https://www.w3.org/Protocols/HTTP/AsImplemented.html)
works.
You cannot upload files (it only supports GET requests), but downloading them
has been confirmed working on NCSA Moasic 1.0.3 for Mac.

Of course, not every browser that had support for HTML 3.0 had full support for
it.
Internet Explorer 3 is one such example: although you can tell it to request
HTML 3.0, and you can actually pick a file to upload, it is unable to upload
the file.

![Internet Explorer 3 failing to upload a file to SPFS on Mac OS 9](docs/img/ms_ie/105-ie-3.01a-mac.png "Internet Explorer 3 failing to upload a file to SPFS on Mac OS 9")

Whether this was the result of a buggy implementation or if Microsoft just
didn't implement it is unclear.
And we think this is a mystery that will remain unsolved, as these discussions
likely took place on mailing lists that have been lost to time.
(And yes, we have checked Google's Usenet archive. You find some
[interesting](https://groups.google.com/g/comp.sys.mac.apps/c/YPyrO7YpMs0/m/rUbc_C_m7R8J)
[nuggets](https://groups.google.com/g/microsoft.public.inetexplorer.ie3/c/xAEGKxYDsWo/m/TC5WDnYU0YQJ)
in there, but nothing that seems relevant to this issue.)

### Minimum Supported Browsers

These are the earliest versions of compatible browsers which have been confirmed
to work with SPFS:

* Microsoft Internet Explorer 4.0
* Netscape Navigator 2.0

You can find downloads for these and other old browsers at the following
locations:

* [ALTEXXANET](http://altexxanet.org/downloads.html)
* [evolt](https://browsers.evolt.org/)
* [Macintosh Garden](https://macintoshgarden.org/)
* [System 7 Today](https://www.system7today.com/internet)

Note that "work" in this context means it is able to list, download, and upload
files.
Many other browsers can list and download files just fine, but not upload.
And it does not imply that it is guaranteed to work perfectly - some browsers
may have trouble with certain filetypes or be unable to render certain
characters (such as the copyright symbol), but ultimately, this is the bare
minimum you can use for full functionality.

A full breakdown can be found in the
[compatibility details file](docs/COMPATIBILITY.md), which includes a helpful
[chart](docs/COMPATIBILITY.md#compatibility-chart) you can use.
It also contains an overview of many old browsers and any quirks observed
in them that would be useful if you want to develop applications targeting
them.

## Notes/FAQ

> Why did you make this?

Let's say you are running Windows 98 in VirtualBox (or any other OS that
VirtualBox does not provide Guest Additions for) and need to send files to or
from the VM (or between several 98 VMs).
How can you do this?

Windows 98 cannot connect to modern SMB shares and certainly does not support
shared folders (that's a Guest Additions feature,
which only supports NT-based Windows).
Most websites (including file uploading ones) are not designed to work with
browsers that run on Windows 98, so your only option is to fire up an FTP
server.
CDs are inconvenient to move files with (floppies even more so with their
size limitations), and good luck getting USB mass storage devices working
reliably on older OSs.

Or, you can just fire this up and use the pre-installed IE in your Windows 98 VM,
no legacy FileZilla or physical media needed!

![SPFS running in Internet Explorer 5 on Windows 98](docs/img/ms_ie/003-ie-5.0-win.png "SPFS running in Internet Explorer 5 on Windows 98")

This may seem to be an obscure use case, but it is what caused me to create
this application.  
For a more common use case, let's say you are working on remediating a
malware-infested machine and you do not feel comfortable inserting a USB drive
into it (or you cannot insert a USB drive, e.g. a tablet without USB-OTG or
with a bad battery that relies on the charger), but need to extract some files.
What are your options?

* You could connect to an FTP/SMB server you control, but you risk your
  credentials being scooped up by a keylogger.
* Going to an anonymous file upload website, where you'll have to deal with
  upload speed and file size limitations, along with the knowledge that anyone
  can access it. In addition, this requires exposing the machine to the
  internet, instead of letting it remain safely on an isolated VLAN.
* Pulling the drive and putting it into an external reader to ensure no malware
  can execute on it is great when you are physically able and allowed to
  deconstruct the device, but you might not have permission, the necessary tools,
  the necessary filesystem drivers (e.g. btrfs drive connected to Windows),
  the hardware keystore to decrypt it once removed, or the device has soldered
  storage (i.e. Macbooks and every modern mobile device).

There are a variety of obscure reasons why your only way to get files off of a
device is the network, and this application is designed for that specific
purpose. It is:

* Anonymous
* Basic (the barebones webpages should have minimal rendering issues even in
  early-90s browsers)
* Easy to setup
* Has no file size or speed limitations (except that of your hardware)
* Stateless - no cookies, no database, just a config file and a webserver

> What is the purpose of the `allow_all` variable in the config file?

If you do not feel like enumerating all of the allowed file extensions (or are
dealing with, e.g. classic Mac OS, where applications oftentimes did not add
file extensions, instead relying on the resource fork for that), set `allow_all`
to True and all files will be accepted, regardless of whether or not a given
file has an extension that has been explicitly allowed.

> Can I expose this application to the Internet?

Yes, but that is a *terrible* idea.
This is intended to only be used on isolated networks.
Ignoring the security concerns, the
[Flask documentation](https://flask.palletsprojects.com/en/1.1.x/tutorial/deploy/#run-with-a-production-server)
states:

> > When running publicly rather than in development, you should not use the
> > built-in development server (flask run). The development server is provided
> > by Werkzeug for convenience, but is not designed to be particularly
> > efficient, stable, or secure.

So if you really want to expose it to the internet, take a look at their guide
on
[production deployment](https://flask.palletsprojects.com/en/1.1.x/deploying/).

> What port does this run on?

By default, Flask runs on port 5000.

> I want to access this outside localhost. How do I get it to listen on 0.0.0.0?

Append `--host=0.0.0.0` to your `flask run` command.

> How can I change the port this runs on?

Append `--port=[your port]` to your `flask run` command.

Alternatively, you can set the `FLASK_RUN_PORT` variable (note that you *must*
do `export FLASK_RUN_PORT=####`, if you leave off the `export` it will not
work), and `flask run` will automatically use the port you set.

> How can I make this application autostart with my machine?

If you are using anything besides a systemd Linux distro, I don't know.
Feel free to submit an MR if you know how.

On distros that use systemd, the process is quite simple:

```bash
cd app/
# Set variables
APPUSER=username_here # Note that this user should ideally be able to write to both the SPFS application directory and uploads directory
APPDIR=$(pwd) # This is the working directory of the application, not the uploads directory; if the application will actually be located elsewhere, be sure to set this variable appropriately
# If using custom args (see below):
SPFSARGS="my args"
# Edit unit file
sed "s?\?APPUSER\??${APPUSER}?g; s?\?APPDIR\??${APPDIR}?g; s?\?SPFSARGS\??${SPFSARGS}?g; " spfs.service.example > spfs.service
# Move unit file and start it
# You might need to prepend these last commands with sudo
cp spfs.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable spfs.service
systemctl start spfs.service
```

The `SPFSARGS` variable is for any parameters you want to feed to the
application, e.g. `--host` and `--port`. If you are not using it, do not set
this variable.
It acts the same regardless of if `SPFSARGS` is unset or blank.

The only potential footgun here is that you *must* run `pipenv install` from
`APPUSER`'s account or the necessary dependencies will not be present and the
application will be unable to start.
So ideally, you'd run `pipenv install`, then `APPUSER=$USER` before setting
`APPDIR`, `SPFSARGS`, etc.

Once you have run these commands, SPFS will now automatically start with the
system.
You can use `systemctl disable spfs.service` to disable this later on.

> Why does the pipfile allow for any Python 3 version if the application will
> not run on <3.8?

Currently, pipenv only supports exact versions
([source](https://github.com/pypa/pipenv/issues/2694)). Until one can specify
a minimum version, we are stuck with just requiring Python 3 in the pipfile -
if I were to do `python_version = "3.8"`, pipenv would refuse to work if you
did not have 3.8 installed (i.e. it would refuse to work with 3.9+).

> Is this application secure?

Probably not very secure.
This application does not use CSRF protection, is anonymous, and has minimal
server-side file checking.

> Do you have plans to make it look better?

No.
Compatibility with older browsers (like IE5 on Windows 98 and IE4 on Mac OS 9)
was part of the design goals for this application, even just using CSS will run
into many compatibility problems (including hard-locking...yes, hard-locking the
system with CSS, that's a thing you have to worry about on early IE for Mac
versions).
This application is intentionally barebones.
And trying to write JS for IE4?
No thanks.
(It didn't even [have the DOM](https://alistapart.com/article/ie5mac/).)

> Do you have plans to make this application able to delete files?

No.
One use case of this is extracting files from malware-infested machines,
and I do not want to risk someone writing a tool to delete all files on the
server through the web interface.
This application prepends a random string to the names of uploaded files so a
malicious user cannot overwrite files, either.

Just use a file browser or CLI on the server to manage uploaded files.

> Does PyPI `python-magic` interfere with the `python-magic` and `python3-magic`
> packages available on many Linux distros?

This application uses pipenv, which keeps its installed packages separate from
any system libraries and system/user pip installs, and gives priority to its
own packages when they conflict.
So no, this does not interfere with any system packages.

> I cannot upload some files unless I rename them to be shorter.
> Their names are not excessively long, nor am I running into storage or
> permissions limits.
> Why is this?

On most filesystems I have looked at, the limit for filenames is 255 bytes.
However, looks can be deceiving.

If you are storing files in an encrypted home directory (via eCryptfs), your
limit is actually [143 bytes](https://bugs.launchpad.net/ecryptfs/+bug/344878),
as the remaining 112 are reserved for metadata.

This application also prepends a randomized string (23 characters) to filenames
to ensure files cannot be easily overwritten, so your limit may only be 120 or
232 bytes (and then there's also the OS path-length limit to consider).

The other possibility is that your browser is including the entire path as part
of the filename when it uploads files, so a relatively short filename easily
ends up exceeding 120 or 232 bytes (see the
[compatibility chart](#compatibility-chart) for more information on this).

> My browser is having problems because of the UTF-8 enforcement.
> How do I deal with this?

Navigate to `/?force_utf8=False` and your browser's user-agent string will be
automatically blacklisted, and the application will use the ISO 8859-1 charset
for all future requests.

Some browsers have been blacklisted already because of issues related to this.
Feel free to submit an issue with any problematic user-agent strings so they
can be added to the default blacklist for all users to benefit from.

> I blacklisted a browser from receiving explicit UTF-8/Unicode tags via the
> `/?force_utf8=False` URL in an attempt to solve a problem, but it didn't
> work, and now I want to remove the browser from that list.
> How can I do this?

You will have to manually edit the user blacklists file
(`app/utf8_blacklist_additions.json`) to remove the user-agent string.

Since this application might be used in environments where you cannot just put
an "undo" feature out there and expect it to not be abused, this application
takes the same stance as deleting files: just have the admin do it (which is
probably you since this is a pretty niche application).

Also, chances are re-enabling it won't fix your problem unless you're in
[IE 5-8](https://stackoverflow.com/a/3348524).

## Versioning

This project uses Semantic Versioning, aka
[SemVer](https://semver.org/spec/v2.0.0.html).

## Contributing

If you would like to suggest features, report bugs, etc., feel free to open an
issue.

If you would like to help develop the application, merge requests are welcome.
For major changes, please open an issue first to discuss what you would like
to change.

### Localization

This application is currently available in the following languages:

* English

If you would like to make this application available in your langauge, feel
free to submit a merge request with translations.

To create translations, please follow these steps (taken from
[these docs](https://flask-babel.tkte.ch/#translating-applications)):

1. Inside the [application file](app/spfs.py), add the
   [ISO 639-1 code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)
   and name to the `app.config['LANGUAGES']` dict.
2. Execute the following commands:
   ```bash
   cd app/
   pipenv shell
   # Extract strings
   pybabel extract -F babel.cfg -o messages.pot --input-dirs=.
   # Create PO file (the file with the strings to translate)
   pybabel init -i messages.pot -d translations -l INSERT_639-1_CODE_HERE
   ```
3. Edit the generated PO file (take a look at the English file for the comment
   block and translation info string), then run this command:
   ```bash
   pybabel compile -d translations
   ```
4. Test the translations by starting up the application and using it.
   If possible, I'd recommend testing with very old browsers as well to make
   sure the more obscure errors (e.g. HTML 3.0 required) show up.
   However, anything outside ISO 8859-1/Latin-1 (which should render reliably
   on any browser released after 1987 (which is all of them) since the first
   256 Unicode characters are Latin-1) will likely run into some trouble for
   pre-Unicode browsers (e.g. IE4), and there seems to be nothing that can be
   reasonably done to handle alternate encodings.

## Authors and Acknowledgments

* Luna Lucadou - Wrote the application in her free time
* eientei95 - Helped diagnose HTTP/MIME implementation errors in IE3 for Mac
  ([seen here](https://gitlab.com/lucadou/spfs/-/issues/9#note_952180833))
* evolt team - Hosts an archive with many old browsers available for download
  at https://browsers.evolt.org/

## License

This project is licensed under the GNU General Public License v3.0 - see the
[LICENSE.md](LICENSE.md) file for details.
