# SPFS Compatibility Notes

During the process of debugging this application on older browsers,
many challenges have been encountered, which shall be documented here
for those interested and also for those with an interest in developing
for the Old Web, including notes about browser-specific quirks that have
been encountered.

## Relevant Documentation

The following documents will be of use to anyone creating web applications
for older web browsers:

* HTML
  * (Original) HTML info (1991): http://info.cern.ch/hypertext/WWW/MarkUp/Tags.html
  * HTML+ info (1993): https://www.w3.org/MarkUp/htmlplus_paper/htmlplus.html
    * HTML+ spec (1993): https://www.w3.org/MarkUp/HTMLPlus/htmlplus_1.html
  * HTML 2.0 spec (1995): https://www.w3.org/MarkUp/html-spec/html-spec_toc.html
  * HTML 3.2 spec (1996): https://www.w3.org/TR/2018/SPSD-html32-20180315/
  * HTML 4.0 spec (1998): https://www.w3.org/TR/1998/REC-html40-19980424/
  * HTML 4.1 spec (1999): https://www.w3.org/TR/html401/
* HTTP
  * HTTP/0.9 spec (1991): https://www.w3.org/Protocols/HTTP/AsImplemented.html
  * HTTP/1.0 spec (1996): https://datatracker.ietf.org/doc/html/rfc1945
  * HTTP/1.1 spec (1999): https://datatracker.ietf.org/doc/html/rfc2616

It is important that anyone considering developing for older browsers consider
what features they will be using and figure out when those features were
introduced, as that will let you figure out what browsers you can target.

## Practical Considerations

Looking at specs can be deceiving, however.
You may already be aware of the
[Browser Wars](https://en.wikipedia.org/wiki/Browser_wars#First_Browser_War_(1995%E2%80%932001)).
You may have also seen these while trawling old websites in the Internet
Archive:

![A screenshot of two "Best viewed with" images, one for Internet Explorer and the other for Netscape 3.0](img/best-viewed-with.png)

These may seem like amusing footnotes today, but due to the massively diverging
implementations during the browser wars, they were a necessity for anyone doing
anything even remotely complicated.

But even simple pages that comply with the HTML 2.0 spec can render differently
between releases of one browser, including releases of the same version on
different operating systems.

Internet Explorer 5 for Mac, for instance, used the
[Tasman engine](https://en.wikipedia.org/wiki/Tasman_(browser_engine)),
which was far more spec-compliant than IE5 for Windows, which used
[MSHTML](https://en.wikipedia.org/wiki/MSHTML).
And not only did the two IE5 releases render differently, they also had large
feature gaps (e.g. many ActiveX features were Windows-exclusive).

All this to say: don't expect an easy ride, and take claims of spec-compliance
with a grain of salt.

If you want to target older browsers, you should also consider the minimum HTTP
version you will support and if your application can be implemented entirely
on the server-side or if you will need interactive elements.
Static webpages can be implemented using HTTP/0.9 (and SPFS can list and
download files using just HTTP/0.9), but if you cannot express your application
using just GET requests, you will have to target HTTP/1.0 or newer.

Interactive elements should avoid JavaScript if at all possible.
The older the browsers you target, the worse the experience gets, especially
if you want your users to be able to access your application on
period-appropriate hardware.
You should probably go out and buy period-appropriate hardware if you intend
on doing this.
Some of the problems you may encounter include:

* If you load lots of images (or just a few large ones), your OS might start
  using swap space on old hard drives
* Loading pages over LAN seems like it would be fast until you consider you
  have a 10 Mbps half-duplex NIC
* If you generate SSL certificates that will work with your target browsers
  (which will be massively insecure since you'll be using weak algorithms,
  but regardless, to continue the hypothetical), the older your hardware,
  the longer it will take to establish a connection for every single file
  on the page
* CSS and JS diagnostic tools are primitive
* Messing up your CSS and JS can result in hard-locking your system
  * This is just the result of older browsers having less complex control
    mechanisms combined with the realities of cooperative multitasking
* Cache-control is easy if you're using something like Rails, which includes
  cache-busting as a standard feature, but otherwise you're at the mercy of
  the HTTP implementation

## Problems Encountered

### HTTP/1.0 and "HTTP/1.0"

One of the key differences between HTTP/1.0 and 1.1 (as far as this application
is concerned) is the addition of the
[Host header](https://datatracker.ietf.org/doc/html/rfc2616#section-14.23).
This is a very helpful feature that is widely used by nginx, Apache, etc. to
allow multiple services to listen on the same port, with the reverse proxy
routing traffic to each server block (nginx) or virtual host (Apache) based on
the contents of that header.

Without the `Host` header, all traffic that goes to a reverse proxy will go to
the default server/host, which is typically a static "this webserver needs to be
configured" message.

![Internet Explorer 2.0.1 for Mac with the stock "Welcome to nginx!" page because IE2 does not include Host headers](img/ms_ie/102-ie-2.0.1-mac-3-host-header-demo.png)

If you want the application to be used by browsers that do not include a `Host`
header, you will need to configure the
[default_server parameter](http://nginx.org/en/docs/http/ngx_http_core_module.html#listen)
(nginx), the
[default/primary VirtualHost](https://httpd.apache.org/docs/2.4/vhosts/examples.html#purename)
(Apache), or your webserver's equivalent.

Of course, if you are just exposing Flask directly on port 80 (or another port),
you do not need to worry about any of this, but for those who wish to run SPFS
behind another webserver with other applications running, this is something to
take into consideration.

But something else to keep in mind is that the HTTP spec was not always
implemented correctly.

Netscape Navigator 2 is the earliest browser (that we have found thus far) which
uses the `Host` header.
It was released on September 18, 1995, at which point HTTP/1.0 was on its
[third draft](https://datatracker.ietf.org/doc/html/draft-ietf-http-v10-spec-03),
and it sends HTTP/1.0 requests.
It would be close to two years before the
[first HTTP/1.1 draft](https://datatracker.ietf.org/doc/html/draft-ietf-http-v11-spec-rev-00)
was released.

Internet Explorer began doing this in version 3.0 (released January 8, 1997),
also before the first HTTP/1.1 draft.

As browsers began adopting HTTP/1.1, they typically left their HTTP/1.0
implementations as-is, which means browsers that let you toggle between
HTTP/1.0 and 1.1 will most likely not have a spec-compliant 1.0 implementation,
which can cause very difficult to track down bugs in the 1.0-compliant browsers
(see issue #9 for one such bug).

All this to say that it is very easy to become bogged down with bugs triggered
by spec-compliance.
When building for old browsers, you should be careful even with
seemingly-innocuous choices like using a method which returns a URL that
includes the hostname, because when a client connects and does not submit the
`Host` header, it will break your application.

## Compatibility Chart

This is what we know for sure about browser compatibility:

| Browser                                    | OS      | List Files | Download Files | Upload Files | Other Thoughts                                                                                                                                                                                                 |
|--------------------------------------------|---------|------------|----------------|--------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| NCSA Mosaic 1.0 (1993)                     | Windows | Yes        | Yes            | No           | * Tested on Windows 98 in Connectix Virtual PC<br>* Copyright symbol not rendered properly<br>* Tables not supported<br>* Must enable Options>Load to Disk to save files                                       |
| NCSA Mosaic 2.0 Alpha 2 (1993)             | Windows | Yes        | Yes            | No           | * Tested on Windows 98 in Connectix Virtual PC<br>* Copyright symbol not rendered properly<br>* Tables not supported<br>* Must enable Options>Load to Disk to save files                                       |
| MacWWW 1.03 (1993)                         | Mac     | Yes        | No             | No           | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Tables not supported                                                                                                                    |
| MacWeb 0.98 Alpha (1994)                   | Mac     | Yes        | Yes            | No           | * Footer does not render at all<br>* Tables not supported<br>* Downloads can be quite finicky for some filetypes (especially images)                                                                           |
| NCSA Mosaic 1.0.3 (1994)                   | Mac     | Yes        | Yes            | No           | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Copyright symbol not rendered properly<br>* Tables not supported                                                                        |
| NCSA Mosaic 1.0.3, using HTTP/0.9 (1994)   | Mac     | Yes        | Yes            | No           | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Copyright symbol not rendered properly<br>* Tables not supported                                                                        |
| Netscape 0.93 Beta (1994)                  | Mac     | No         | No             | No           | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Throws TCP errors                                                                                                                       |
| Netscape 0.96 Beta (1994)                  | Mac     | No         | No             | No           | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Throws error -14                                                                                                                        |
| Netscape 1.0N (1994)                       | Mac     | Yes        | Yes            | No           | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Tables not supported                                                                                                                    |
| MacWeb 1.1.1E (1995)                       | Mac     | Yes        | Yes            | No           | * Copyright symbol not rendered properly<br>* Tables not supported                                                                                                                                             |
| NCSA Mosaic 2.0.1 (1995)                   | Mac     | Yes        | Yes            | No           | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Text is annoyingly small                                                                                                                |
| Netscape Navigator 2.0 (1995)              | Mac     | Yes        | Yes            | Yes          | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Butchers special characters in filenames when uploading (e.g. spaces -> "20")                                                           |
| Netscape Navigator 2.01 (1995)             | Mac     | Yes        | Yes            | Yes          | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Butchers special characters in filenames when uploading (e.g. spaces -> "20")                                                           |
| Netscape Navigator 2.02 (1995)             | Mac     | Yes        | Yes            | Yes          | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Butchers special characters in filenames when uploading (e.g. spaces -> "20")                                                           |
| Internet Explorer 2.0.1 (1996)             | Mac     | Yes        | Yes            | No           | * Text is annoyingly small ([fixed in IE5 for Mac](https://alistapart.com/article/ie5mac/#section3))                                                                                                           |
| Internet Explorer 3.0.1 (1996)             | Windows | Yes        | Yes            | No           | * Tested on Windows 95 in VirtualBox                                                                                                                                                                           |
| Internet Explorer 3.02 (1996)              | Windows | Yes        | Yes            | No           | * Tested on Windows 95 in VirtualBox                                                                                                                                                                           |
| MacWeb 2.0 (1996)                          | Mac     | Yes        | Yes            | No           | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)                                                                                                                                              |
| NCSA Mosaic 3.0 (1996)                     | Windows | Yes        | Yes            | No           | * Tested on Windows 98 in VirtualBox                                                                                                                                                                           |
| NCSA Mosaic 3.0.0 Beta 2 (1996)            | Mac     | Yes        | Yes            | No           | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Text is annoyingly small                                                                                                                |
| NCSA Mosaic 3.0.0 Beta 3 (1996)            | Mac     | Yes        | No             | No           | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Text is annoyingly small<br>* Crashes and takes down the whole system                                                                   |
| NCSA Mosaic 3.0.0 Beta 4 (1996)            | Mac     | Yes        | No             | No           | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Text is annoyingly small<br>* Crashes and takes down the whole system                                                                   |
| UdiWWW 1.2 (1996)                          | Windows | Yes        | Yes            | Yes          | * Tested on Windows 98 in Connectix Virtual PC and Windows 2000 in KVM<br>* Copyright symbol not rendered properly<br>* File uploads only work reliably for text files (including base64-encoded binary files) |
| Internet Explorer 3.01a (1997)             | Mac     | Yes        | Yes            | No           | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Text is annoyingly small ([fixed in IE5 for Mac](https://alistapart.com/article/ie5mac/#section3))                                      |
| Netscape Navigator 3.04 Gold (1997)        | Mac     | Yes        | Yes            | Yes          | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Butchers special characters in filenames when uploading (e.g. spaces -> "20")                                                           |
| Netscape Navigator 3.04 Gold (1997)        | Windows | Yes        | Yes            | Yes          | * Copyright symbol not rendered properly<br>* Uploaded file names include full path (and converted to [8.3](https://en.wikipedia.org/wiki/8.3_filename))                                                       |
| Netscape Communicator 4.04 Pro (1997)      | Mac     | Yes        | Yes            | Yes          | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Butchers special characters in filenames when uploading (e.g. spaces -> "20")<br>* Generally unstable browser                           |
| Internet Explorer 4.0 (1998)               | Mac     | Yes        | Yes            | Yes          | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Text is annoyingly small ([fixed in IE5 for Mac](https://alistapart.com/article/ie5mac/#section3))                                      |
| Internet Explorer 4.01 (1998)              | Mac     | Yes        | Yes            | No           | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Text is annoyingly small ([fixed in IE5 for Mac](https://alistapart.com/article/ie5mac/#section3))                                      |
| Netscape Communicator 4.08 Complete (1998) | Mac     | Yes        | Yes            | Yes          | * Tested on System 7.6.1 in Basilisk II (emulated Macintosh IIci)<br>* Butchers special characters in filenames when uploading (e.g. spaces -> "20")<br>* Slightly more stable than 4.04                       |
| Internet Explorer 5.0 (1999)               | Windows | Yes        | Yes            | Yes          | * Tested on Windows 98 in Connectix Virtual PC<br>* Copyright symbol not rendered properly<br>* Uploaded file names include full path                                                                          |
| Internet Explorer 5.0 (2000)               | Mac     | Yes        | Yes            | Yes          | * Works exactly as expected                                                                                                                                                                                    |
| Netscape 7.0.2 (2003)                      | Mac     | Yes        | Yes            | Yes          | * Works exactly as expected                                                                                                                                                                                    |
| Opera 10.10 (2009)                         | Windows | Yes        | Yes            | Yes          | * Tested on Windows 98 in Connectix Virtual PC<br>* Works exactly as expected                                                                                                                                  |
| Classilla 9.3.1 (2012)                     | Mac     | Yes        | Yes            | Yes          | * Works exactly as expected<br>* What I intially used to test the application                                                                                                                                  |
| Mozilla Firefox 10.0.12 (2013)             | Windows | Yes        | Yes            | Yes          | * Tested on Windows 98 in VirtualBox<br>* Works exactly as expected                                                                                                                                            |
| IBrowse 2.5.5 (2021)                       | Amiga   | Yes        | Yes            | Yes          | * Tested on an Amiga 4000 running AmigaOS 3.9 in WinUAE<br>* Works exactly as expected                                                                                                                         |

Testing setup:

* Anything run under Connectix Virtual PC was run on a PowerMac G3-233 with Mac
  OS 9.2.2 with Virtual PC 6.0
* Anything run under VirtualBox was run on an x86_64 desktop running Linux Mint
  (Ubuntu)
* Anything run under Basilisk II was run on an x86_64 desktop running Linux Mint
  (Ubuntu)
* Anything run under WinUAE was run on an x86_64 desktop running Linux Mint
  (Ubuntu)
* Anything run under KVM was run on an x86_64 desktop running Linux Mint
  (Ubuntu)
* If no virtualization software was specified, it was run on a PowerMac G3-233

You can see screenshots of all of the above browsers in the
[images folder](img).
(Successful uploads get numbers from 000-099, unsuccessful uploads get 100-199.)


## Browser Quirks

As has been mentioned previously, just because something claims to be
spec-complaint does not mean it actually is.
This application is about as barebones as you can get, so don't expect any
CSS or JS resources.
In fact, you would be well-advised to avoid using CSS and JS because:

1. People who use older browsers tend to disable them for performance and
   stability
2. Documentation for older implementations of CSS and JS are difficult to
   locate, even for people experienced in trawling the Wayback Machine
3. Debugging CSS and JS in old browsers is...
   [painful, to put it lightly](http://www.codestore.net/store.nsf/unid/DOMM-4UZHEY/)

With that disclaimer out of the way, here are some of the quirks encountered
during the development of this application:

### MacWeb

* It seems that MacWeb attempts to recognize files that might have a system
  handler and prompts you to open them externally
* When you download a file that MacWeb thinks can be opened externally, it
  prompts you to select the application ('Ok') or not ('Cancel')
  * (Hitting 'Cancel' still downloads the file but discards it instead of
    opening it)
* If you select 'Ok', you are prompted to select the application to open with
  * On 0.98, this is broken, and it will keep asking you what application until
    you give up and hit 'Cancel'
  * On 1.1.1E, it will successfully open the file, and if the application it
    opened in has a "Save" feature, you can use that to save the file
  * If your system does not have an application that can handle the file, it
    seems there is nothing you can do
* If MacWeb does not think your system has an application that can open the
  file, it will prompt you with a file dialog to choose where to save it

#### MacWeb 2.0

MacWeb was the first browser tested that was old enough to not support tables.
In version 2.0, it began to support "documents that are written with HTML 2.0
plus the Netscape extensions for tables, centering, background images, and
font sizing."
However, version 2.0 doesn't seem to run very well on OS 9, always crashing
during startup.
It runs decently on System 7, though.

When building forms to be submitted with MacWeb 2.0, keep in mind that you need
the full URL in the `action` attribute.
If you provide just a path (e.g. `/uploads/new`), it will submit your form to
`http://0.0.0.0:[port]/uploads/new`.

![MacWeb error message from POSTing upload form "Error 500 Can't Access Document\nhttp://0.0.0.0:5000/uploads/new\nReason: System call `connect` failed:\nunknown error\nT"](img/macweb/104-macweb-2.0-mac-3.png)

As [mentioned above](#http10-and-http10), you cannot rely on methods like
`request.uri` since they depend on the `Host` header (which this does not
provide), so if you wanted to target MacWeb 2.0, you would need to have a
wrapper method that automatically checks for listening addresses and substitutes
them with the appropriate hostname or IP address.

### Microsoft Internet Explorer

#### MSIE 2.0

Though your first instinct may be to expose SPFS directly to your (V)LAN,
it can be fronted by other webservers, such as Nginx.

This is not without its pitfalls, however, as although the `Host` parameter
began to be included well before the release of the
[HTTP/1.1](https://datatracker.ietf.org/doc/html/rfc2616#section-14.23)
spec, it was not universal.

IE2 was released in 1996 and, although Netscape was including the `Host`
header in Navigator 2 (released in 1995),
Microsoft had not yet begun to copy them.

Since Nginx matches the `Host` field against `server_name` directives
([source](http://nginx.org/en/docs/http/request_processing.html)),
it is unable to route requests from IE2 except to the default server.

![Internet Explorer 2 displaying the "Welcome to nginx!" page instead of SPFS on Mac OS 9](docs/img/ms_ie/101-ie-2.0.1-mac-2.png 'Internet Explorer 2 displaying the "Welcome to nginx!" page instead of SPFS on Mac OS 9')

#### MSIE 3.0 for Mac

Though MSIE 3.0 for Windows does not support `file` form fields whatsoever,
3.0 for Mac attempts to.
It gets so close...and gets tripped up by something so subtle, it's genuinely
heartbreaking, because it would have been awesome to get it to work.

Huge shoutout to eientei95 on the Macintosh Garden Discord server for helping
to figure this one out, it's a super simple bug that was extremely easy to miss,
and I bet several developers at Microsoft tore their hair out about this bug
to no avail back in 1997, too.

> The boundary delimiter line is then defined as a line consisting entirely
> of two hyphen characters ("-", decimal value 45) followed by the boundary
> parameter value from the Content-Type header field, optional linear whitespace,
> and a terminating CRLF.

That is, you should define it as `boundary=my-boundary` and then do
`--my-boundary\r\nContent-Disposition: form-data; name="_snowman_"\r\n\r\n`, or
`boundary=my-boundary\r\n--my-boundary\r\nContent-Disposition: form-data; name="_snowman_"\r\n\r\n`.

What IE3 does is
`boundary=--my-boundary\r\n--my-boundary\r\nContent-Disposition: form-data; name="_snowman_"\r\n\r\n`.

In case you missed it, the expected behavior is to *not* include the two hyphens
in the `boundary` definition, as the purpose of the hyphens is to indicate when
the boundary is in use outside its definition.

It was this subtle bug that completely broke IE3 for Mac's MIME implementation.

![Internet Explorer 3.01 error message from POSTing upload form "HTTP error. The server cannot fulfill the request."](img/ms_ie/107-ie-3.01a-mac-3.png)

And because the MIME section is improperly formatted,
it gets dropped before it is ever received by the Flask application,
so there is no way to provide a workaround at this level of abstraction.
It may be possible to fix this by hex-editing the binary
(if you do this, please file an issue so we can update the compatibility table),
but that is outside the scope of this project.

#### MSIE 4.0 for Mac

Internet Explorer 4.0 works for all three primary functions.
However, when uploading a file, if said file is not on your primary drive (or
rather, the drive IE is installed on), it will crash with the message
[error type 3](img/ms_ie/110-ie-4.0-mac-crash.png).

#### MSIE 4.01 for Mac

Considering the success of its predecessor, you would be forgiven for assuming
that Internet Explorer 4.01 for Mac could upload files successfully.
However, for some reason, clicking the Upload button does nothing.
No HTTP request is sent, not even a POST with no file.

It's not that IE 4.01 is unable to POST a form (Macintosh Garden's search
works just fine), it just doesn't work for this,
and we have yet to figure out why.
Even when modifying the form and changing the file field to be a text field,
it does not work,
even though it becomes functionally identical to Macintosh Garden's search form.

### NCSA Mosaic

#### Mosaic 3.0 Beta for Mac

NCSA Mosaic 3.0.0 Beta 3 and 4 for Mac can typically only load the homepage
once.
After that, if you do not delete the "NCSA Mosaic" folder inside
"System Folder > Preferences," the browser will crash and take the rest of
the system out with it the next time you load the homepage.

Don't even bother trying to download any files or access the uploads page,
it will crash everything if you try.

### Netscape

#### Navigator 0.9x Beta for Mac

Netscape Navigator 0.93 and 0.96 beta throw errors when attempting to load the
application.

We suspect this is because they don't know how to handle an HTTP/1.1 response,
even if the response contains nothing they can't handle,
as HTTP/1.0 responses seem to load just fine in them.

Unfortunately, there is no way to force HTTP/1.0
[in Nginx](https://forum.nginx.org/read.php?2,283037,283041),
nor does there appear to be a way to do so in Flask's default webserver.
And replying to HTTP/1.0 with HTTP/1.1 is
[spec-compliant behavior](https://datatracker.ietf.org/doc/html/rfc2145#section-2.3)
(it's up to the client to be able to handle it),
so we doubt a feature request to force HTTP 1.0 would get a serious response
from the Flask developers.
Sadly, it seems these two cannot handle HTTP/1.1, so our hands are tied.
Any browser that depends on HTTP/1.0 will be unable to use this application.

#### Navigator/Communicator 2-4 for Mac

Netscape Navigator 2.01 and 3.04, and Communicator 4.04, and 4.08
all butcher special characters in filenames.
It's like they were encoded on the client side but had the % cutoff (space ->
"%20" becomes "20").
That is what we assumed happened, as we cannot see any other reason why that
would occur.

Netscape Communicator 4.04 and 4.08 (the last releases for System 7) are not
very stable.
Communicator 4.04 tends to hard-lock when attempting to display JPEGs
(even with CSS and JS disabled)
and 4.08 tends to hard-lock if you've downloaded a bunch of files.
Use 4.08 and exit the browser every few downloads for best stability
(or, better yet, just run an older version, like 3.04).

#### Netscape Testing Notes

Many versions of Netscape Navigator and Communicator for Mac are tested in
the Basilisk II emulator because,
while it should run on the PowerMac we usually use for testing (OS 9.2.2),
running multiple Netscape versions tends to mess with the other installs.

Even running Classilla in a separate profile from Netscape 7.0.2, each will
refuse to start while the other is active, and when you go back to Classilla,
since Netscape reset the default profile to its own, you have to manually force
Classilla to use its own profile, all the while risking corruption in the
Netscape profile.

The way we see it, the bigger the gap between releases, the higher the chance
of catastrophic damage to our profiles, so until we get our hands on another
old Mac, old Netscape for Mac versions will be tested in an emulator.

### UdiWWW

This obscure browser was one of the first to advertise its HTML 3.0
compatibility.
However, its implementation was flawed, and led to a very hacky workaround
which is discussed in issue #9.

To make a long story short, it is impossible to fully work around the
broken `file` form implementation without using something lower-level than
Flask, as by the time your application gets to process the request,
Flask has already shoved ASCII and binary data into the same string,
which Python has no good way of dealing with.
As such, any non-ASCII files need to be base64-encoded before being uploaded
via UdiWWW.

In addition, as you can tell from its [screenshots](img/udiwww), it imposes
its own sense of style on websites; CSS need not apply.

## Other Browsers

If you have an old browser and screenshots of SPFS running in it, feel free to
submit an MR to help fill out this table.
