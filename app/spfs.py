#!/bin/python3
"""
This file is part of Simple Python File Server (SPFS),
a simple application for uploading and downloading files
from old and new browsers.
Copyright (C) 2020-2023 Luna Lucadou

SPFS is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

SPFS is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SPFS.
If not, see <https://www.gnu.org/licenses/>.
"""
import datetime
import io
import json
import os
import secrets
from typing import List, Tuple, Union

import flask
import magic
import werkzeug.datastructures
from flask import Flask, flash, make_response, redirect, render_template, request, send_from_directory, url_for
from flask_babel import Babel, format_decimal, gettext
from werkzeug.utils import secure_filename

from app.utils import net_utils, spfs_type_hints

# Load application configuration

conf_data: dict = {}
blacklist_base: List[str] = []
blacklist_additions: List[str] = []
with open('config.json', mode='r') as conf_file:
    conf_data: dict = json.load(conf_file)
conf_file.close()
with open('utf8_blacklist_base.json', mode='r') as utf8_base_blacklist:
    blacklist_base: List[str] = json.load(utf8_base_blacklist)
utf8_base_blacklist.close()
UPLOAD_FOLDER: str = conf_data['upload_folder']
ALLOW_ALL_EXTENSIONS: bool = conf_data['file_extensions']['allow_all']
ALLOWED_EXTENSIONS: List[str] = conf_data['file_extensions']['allowed_extensions']
DISPLAY_FILE_TYPES: bool = conf_data['display_file_types']
MAX_UNIQUE_ATTEMPTS: int = conf_data['file_extensions']['max_unique_file_attempts']
# Number of times the script will try to find a unique name for an upload
# before giving up and showing the user and error message
UNIQUE_TOKEN_BYTES: int = 16
# For prepending to filenames to prevent file overwriting
UNIQUE_TOKEN_LENGTH: int = 23
# Calculated as len(''.join([secrets.token_urlsafe(UNIQUE_TOKEN_BYTES), '-']))

app: flask.app.Flask = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['LANGUAGES'] = {
    'en': 'English'
}
app.secret_key = secrets.token_bytes(32)
# I have to have a secret key for flashes, but since I do not persist any
# data (besides files saved to disk), it can change at any time with no
# side effects.
if conf_data.get('fallback_address'):
    app.config['SERVER_DOMAIN'] = conf_data['fallback_address']
else:
    app.config['SERVER_DOMAIN'] = net_utils.get_local_ip()
# Sets fallback address for clients that need explicit hostnames (see
# net_utils.get_local_ip docstring for more info)
# https://flask.palletsprojects.com/en/2.1.x/api/#flask.Flask.default_config

babel = Babel(app)

@babel.localeselector
def get_locale():
    # https://phrase.com/blog/posts/python-localization-flask-applications/
    return request.accept_languages.best_match(app.config['LANGUAGES'].keys())


@app.route('/', methods=['GET'])
def home() -> flask.Response:
    """
    The homepage of the application. Lists out files, the user-agent, and
    UTF-8/Unicode status.

    :return: the homepage
    """
    if 'force_utf8' in request.args and request.args.get('force_utf8').lower() == 'false':
        set_utf8_status(request.headers.get('User-Agent'))

    print(request.headers)
    print(request.environ)
    print(request.environ.get('SERVER_PROTOCOL') == 'HTTP/1.0')
    print(str(request.headers.get('host')))
    print(request.user_agent)
    print(type(request.headers))

    # https://flask.palletsprojects.com/en/2.1.x/api/#flask.make_response
    # https://flask.palletsprojects.com/en/2.1.x/api/#flask.render_template
    resp: flask.Response = make_response(render_template(
        'index.html',
        files=get_uploads(),
        show_file_type=DISPLAY_FILE_TYPES,
        user_agent=request.headers.get('User-Agent'),
        utf8_status=get_utf8_status(request.headers.get('User-Agent')),
        utf8_status_str=get_utf8_status_str(get_utf8_status(request.headers.get('User-Agent')))
    ))

    return apply_headers(
        resp,
        get_utf8_status(request.headers.get('User-Agent'))
    )


@app.route('/uploads', methods=['GET'])
def upload() -> flask.Response:
    """
    Redirects the user back to the homepage (they shouldn't be on this page
    unless they were messing with the URL).

    :return: a redirect to '/'
    """
    # redirects return Responses, so I can manipulate the headers just like
    # when I do make_response(render_template())
    # https://flask.palletsprojects.com/en/2.1.x/api/#flask.redirect
    resp: flask.Response = redirect(url_for('home'))

    return apply_headers(
        resp,
        get_utf8_status(request.headers.get('User-Agent'))
    )


@app.route('/uploads/new', methods=['GET', 'POST'])
def upload_file() -> flask.Response:
    """
    Given a Request, creates the upload directory, generates a filename,
    and saves the file in the Request.

    :return: Response
    """
    # Type hints from:
    # https://flask.palletsprojects.com/en/2.1.x/api/#flask.render_template
    # https://flask.palletsprojects.com/en/2.1.x/api/#flask.redirect
    if net_utils.get_browser_warnings(str(request.user_agent)):
        for warning in net_utils.get_browser_warnings(str(request.user_agent)):
            flash(warning)
    if request.method == 'POST':
        # Make sure POST has file
        if 'file' not in request.files:
            if net_utils.decode_buggy_file(request.form.get('file'), str(request.user_agent)):
                # Successfully recovered file
                print(gettext('log.info.file.recovery'))
                rf: spfs_type_hints.RecoveredFile = net_utils.decode_buggy_file(
                    request.form.get('file'),
                    str(request.user_agent)
                )
                f_obj: werkzeug.datastructures.FileStorage = werkzeug.datastructures.FileStorage(
                    stream=io.BytesIO(rf.get('data')),
                    headers=request.headers,
                    filename=rf.get('name')
                )
                # This throws errors in my IDE since request.files is a getter,
                # not a setter, but it does work.
                request.files = werkzeug.datastructures.ImmutableMultiDict(
                    [('file', f_obj)]
                )
            else:
                print(gettext('log.info.client.html3'))
                flash(gettext('flash.error.client.html3'))
                resp: flask.Response = redirect(request.url)

                return apply_headers(
                    resp,
                    get_utf8_status(request.headers.get('User-Agent'))
                )
        user_file = request.files['file']
        # Check for filename
        if user_file.filename == '':
            print(gettext('log.info.client.file.missing'))
            flash(gettext('flash.error.client.file.missing'))
            resp: flask.Response = redirect(request.url)

            return apply_headers(
                resp,
                get_utf8_status(request.headers.get('User-Agent'))
            )
        # Make sure filename is valid
        if user_file and file_allowed(user_file.filename):
            # Make sure uploads directory exists
            if not os.path.isdir(app.config['UPLOAD_FOLDER']):
                # Uploads directory does not exist; attempt to create it
                try:
                    os.mkdir(app.config['UPLOAD_FOLDER'])
                except OSError:
                    print(gettext('log.error.server.dir-create-fail'))
                    print(gettext('log.error.server.dir-check-perms %(dir)s',
                                  dir=UPLOAD_FOLDER))
                    flash(gettext('flash.error.server.500'))
                    resp: flask.Response = redirect(request.url)

                    return apply_headers(
                        resp,
                        get_utf8_status(request.headers.get('User-Agent'))
                    )
                else:
                    print(gettext('log.info.server.dir-create-success'))
            # Upload the file

            # Ensure the name is unique
            unique_name: bool = False
            unique_attempts: int = 0
            saved_filename: str = ''
            save_path: str = ''
            while not unique_name and unique_attempts < MAX_UNIQUE_ATTEMPTS:
                unique_attempts += 1
                r_token: str = secrets.token_urlsafe(UNIQUE_TOKEN_BYTES)
                saved_filename: str = r_token + '_' + secure_filename(user_file.filename)
                save_path: str = os.path.join(app.config['UPLOAD_FOLDER'], saved_filename)
                unique_name: bool = not os.path.exists(save_path)
                if unique_name:
                    print(gettext('log.info.server.file.name-generated %(name)s %(num)s',
                                  name=save_path,
                                  num=str(unique_attempts)))
                elif unique_attempts >= MAX_UNIQUE_ATTEMPTS:
                    print(gettext('log.error.server.file.name-generation-fail" %(name)s %(num)s',
                                  name=saved_filename,
                                  num=str(MAX_UNIQUE_ATTEMPTS)))
                    flash(gettext('flash.error.server.file.save-fail'))
                    resp: flask.Response = redirect(request.url)

                    return apply_headers(
                        resp,
                        get_utf8_status(request.headers.get('User-Agent'))
                    )

            try:
                user_file.save(os.path.join(app.config['UPLOAD_FOLDER'],
                                            saved_filename))
                print(gettext('log.info.server.file.save-success %(name)s',
                              name=save_path))
            except FileNotFoundError:
                print(gettext('log.error.file.save-fail %(name)s',
                              name=save_path))
                print(gettext('log.error.server.dir-check-perms %(dir)s',
                              dir=app.config['UPLOAD_FOLDER']))
                flash(gettext('flash.error.server.file.save-fail'))
                resp: flask.Response = redirect(request.url)

                return apply_headers(
                    resp,
                    get_utf8_status(request.headers.get('User-Agent'))
                )
            flash(gettext('flash.info.server.file.save-success %(name)s',
                          name=url_for('uploaded_file', filename=saved_filename)))
            resp: flask.Response = redirect(url_for('home'))

            return apply_headers(
                resp,
                get_utf8_status(request.headers.get('User-Agent'))
            )
    resp: flask.Response = make_response(render_template(
        'upload.html',
        #host=app.config.get('SERVER_DOMAIN'),
        user_agent=request.headers.get('User-Agent'),
        utf8_status=get_utf8_status(request.headers.get('User-Agent')),
        utf8_status_str=get_utf8_status_str(get_utf8_status(request.headers.get('User-Agent')))
    ))

    return apply_headers(
        resp,
        get_utf8_status(request.headers.get('User-Agent'))
    )


@app.route('/uploads/<filename>', methods=['GET'])
def uploaded_file(filename: Union[os.PathLike, str]) -> flask.Response:
    """
    Given a filename, returns the file to the requester via a Response.
    Used for viewing/downloading files.

    If filename is not found, returns 404.

    :param filename: the file to download
    :return: a response with either the file or 404
    """
    # Types from:
    # https://flask.palletsprojects.com/en/2.1.x/api/#flask.send_from_directory
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)
    # This method does not attempt to encode as ISO 8859-1 because the
    # Content-Type header is irrelevant for images, videos, etc.
    # Even if I were to set the headers, however, in the event that the file
    # is not found (404), custom Response headers are dropped in favor of Flask
    # defaults.
    # This means that if a user got a 404 on NCSA Mosaic 3.0 on Windows 98, the
    # browser would crash, but considering that this application will not link
    # to non-existent files to begin with, the user must have been messing
    # with the application to begin with to get a 404.
    # Because of the sequence of events that would have to lead up to a 404,
    # the effort to create a custom error handler and set custom headers to
    # prevent crashing obscure browsers doesn't seem to be worth it.


def file_allowed(filename: str) -> bool:
    """
    Given a filename, determine if it is allowed to be uploaded based on its
    extension.

    :param filename: the name to check
    :return: whether the file is allowed or not
    """
    if ALLOW_ALL_EXTENSIONS:
        return True
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in \
           ALLOWED_EXTENSIONS
    # .rsplit('.', 1) will only split the last occurrence of '.',
    # e.g. 'file.name.txt' is split into ['file.name', 'txt']


def get_uploads() -> List[spfs_type_hints.FileInfo]:
    """
    Gets a list of all uploaded files and returns a list of FileInfo objects
    (TypedDicts), sorted by file modification date with the most recent
    items first.

    :return: a list of FileInfo objects
    """
    # Gets all uploaded files and returns their original names, file size (in
    # the most relevant units), upload (modification) time, and URL.
    raw_files: List[str] = os.listdir(UPLOAD_FOLDER)
    files: List[spfs_type_hints.FileInfo] = []
    for f in raw_files:
        full_path: str = os.path.join(UPLOAD_FOLDER, f)
        size: Tuple[str, str] = get_size(full_path)
        mime_type: str = get_file_type(full_path)
        last_mod_time: float = os.stat(full_path).st_mtime
        # st_ctime is creation time (Windows) or last metadata modified
        # time (Unix), so st_mtime (last modified time on all platforms)
        # is what I am going with (since the file being modified should be
        # more relevant than metadata changing)
        f_info: spfs_type_hints.FileInfo = {
            'name': f[UNIQUE_TOKEN_LENGTH:],  # Remove all the unique characters to get the original name
            'size': gettext('generic.unit.template %(size)s %(unit)s',
                            size=size[0],
                            unit=size[1]),
            'type': mime_type,
            'time': datetime.datetime.fromtimestamp(last_mod_time),
            'url': url_for('uploaded_file', filename=f)
        }
        files += [f_info]
    # Return, sorted with newest items first
    return sorted(files, key=lambda f: f['time'], reverse=True)


def get_size(file_path: str) -> Tuple[str, str]:
    """
    Given a file path, calculates the size in human-readable units.
    Uses units defined by IEC 80000-13 (KiB, MiB, GiB).

    :param file_path: the file to check
    :return: the size of the file and its unit
    """
    size_divisor: int = 1024

    raw_size: int = os.stat(file_path).st_size
    size_kib: float = raw_size / size_divisor
    size_mib: float = size_kib / size_divisor
    size_gib: float = size_mib / size_divisor
    if size_gib > 1:
        return (format_decimal(round(size_gib, 2)),
                gettext('generic.unit.gibibyte'))
    elif size_mib > 1:
        return (format_decimal(round(size_mib, 2)),
                gettext('generic.unit.mebibyte'))
    else:
        return (format_decimal(round(size_kib, 2)),
                gettext('generic.unit.kibibyte'))


def get_file_type(file_path: str,
                  type_checker: magic.Magic = magic.Magic(mime=True)) -> str:
    """
    Given a file path, determines the type of it.
    By default, it will return the file's MIME type, but you can supply any
    Magic object to customize what it returns
    (see: https://github.com/ahupp/python-magic#usage).

    If file type checking is disabled, returns an empty string.

    :param file_path: the file to check
    :param type_checker: the type checker to use (optional, defaults to MIME type checker)
    :return: the type of the file or zero-length string
    """
    # The built-in mimetypes package is not sufficient for this, as it just
    # looks at the file extension, while magic integrates with libmagic.
    # https://stackoverflow.com/a/55319252/15264046
    file_type: str = ''

    if DISPLAY_FILE_TYPES:
        file_type += type_checker.from_file(file_path)

    return file_type


def get_utf8_status(user_agent: str) -> bool:
    """
    Given a user agent, determines whether or not the application should
    explicitly enable Unicode/UTF-8 or if those tags should be left out.

    If the application has been set to use ISO-8859-1 encoding in the config
    file, it will always return false.

    :param user_agent: the user-agent string for the Request
    :return: if UTF-8 is explicitly enabled
    """
    if user_agent in blacklist_base:
        return False

    # Read blacklist additions
    #
    # Note that the base list is stored globally since it being updated would
    # be via git pull, which means the application must be restarted.
    # User additions, on the other hand, require re-reading the file since
    # changes are persisted to it.
    blacklist_additions: List[str] = []
    if os.path.isfile('utf8_blacklist_additions.json'):
        with open('utf8_blacklist_additions.json', mode='r') as utf8_user_blacklist:
            blacklist_additions: List[str] = json.load(utf8_user_blacklist)
        utf8_user_blacklist.close()
    if user_agent in blacklist_additions:
        print(gettext('log.info.server.utf8.blacklist-addition %(user_agent)s',
                      user_agent=user_agent))
        return False
    else:
        return True


def get_utf8_status_str(utf8_status: bool) -> str:
    """
    Gets the appropriate translation string for UTF-8/Unicode being explicitly
    enabled or not.

    :param utf8_status: whether UTF-8 is explicitly enabled
    :return: the appropriate translation string
    """
    if utf8_status:
        return gettext('tmpl.app.footer.utf8.enabled')
    else:
        return gettext('tmpl.app.footer.utf8.disabled')


def set_utf8_status(user_agent: str) -> bool:
    """
    Given a user agent, disables explicit UTF-8/Unicode for it.

    :param user_agent: the user-agent to blacklist
    :return: whether or not the blacklist was updated successfully
    """
    # Read in current blacklist additions
    blacklist_additions: List[str] = []
    if os.path.isfile('utf8_blacklist_additions.json'):
        with open('utf8_blacklist_additions.json', mode='r') as utf8_user_blacklist:
            blacklist_additions: List[str] = json.load(utf8_user_blacklist)
        utf8_user_blacklist.close()

    # Add user-agent
    if not user_agent in blacklist_additions:
        blacklist_additions.append(user_agent)

    # Write to disk
    #
    # The file is sorted because the two blacklists have to be kept sorted
    # to use `comm` on during updates that require pruning the user-blacklist.
    try:
        with open('utf8_blacklist_additions.json', mode='w') as utf8_user_blacklist:
            json.dump(sorted(blacklist_additions), utf8_user_blacklist,
                      indent=2)
            # Indent is to make using `comm` easier - one item per line
        utf8_user_blacklist.close()
        print(gettext('log.error.server.utf8.blacklist.addition-succeeded %(user_agent)s',
                      user_agent=user_agent))
    except Exception as err:
        print(gettext('log.error.server.utf8.blacklist.addition-failed %(user_agent)s',
              user_agent=user_agent))
        print('\t' + err)
        return False
    return True


def needs_hostname(req: flask.Request) -> bool:
    """
    Given a Request, determines if the browser needs the hostname to be
    explicitly set.

    This is based on the HTTP version and the contents of the ``Host``
    header.
    Some HTTP/1.0 browsers get tripped up when you do not provide the
    hostname in links, and this helps reduce errors spawned from their buggy
    implementation.

    :param req: the request object
    :return: whether or not you should explicitly declare hostnames for this
    response
    """
    # https://flask.palletsprojects.com/en/2.1.x/api/#flask.Request.environ
    if is_legacy_http(req) and req.headers.get('Host') is None:
        return True
    return False


def is_legacy_http(req: flask.Request) -> bool:
    """
    Given a Request, determines if the HTTP version is a "legacy" one.

    For the purposes of this method, "legacy" HTTP includes 0.9 and 1.0,
    as they do not usually include the `Host` header (the mid-90s is when
    browsers began breaking the 1.0 spec in this regard, see
    docs/COMPATIBILITY.md for more info).

    Note that HTTP/0.9 requests will be "upgraded" into HTTP/1.0 by some
    reverse proxies (such as nginx).
    You will only see HTTP/0.9 requests in the application if fronted by
    extremely old reverse proxies or if Flask is exposed directly.
    To ensure maximum compatibility, HTTP/0.9 is checked for in addition
    to HTTP/1.0.

    :param req: the Request to check
    :return: whether it was sent via a legacy HTTP method
    """
    legacy_versions: Tuple[str, str] = ('HTTP/0.9', 'HTTP/1.0')
    return req.environ.get('SERVER_PROTOCOL') in legacy_versions


def apply_headers(resp: flask.Response, use_utf8: bool) -> flask.Response:
    """
    Given a Response, sets the appropriate Content-Type headers and returns
    the Response object.

    Note that this may cause some characters to not render properly (especially
    if the application has been translated to languages that use characters
    outside ISO 8859-1 and the connecting browser has requested one of those
    languages), but this is the best way to ensure compatibility with older
    browsers that strictly adhere to the HTTP/1.0 spec (see:
    https://gitlab.com/lucadou/spfs/-/issues/7).

    :param resp: the Response object to modify
    :param use_utf8: whether to use UTF-8 (True) or ISO 8859-1 (False)
    :return: the updated Response object
    """
    if not use_utf8:
        resp.headers.set('Content-Type', 'text/html')
    return resp
