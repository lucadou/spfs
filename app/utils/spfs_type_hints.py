"""
This file is part of Simple Python File Server (SPFS),
a simple application for uploading and downloading files
from old and new browsers.
Copyright (C) 2020-2023 Luna Lucadou

SPFS is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

SPFS is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SPFS.
If not, see <https://www.gnu.org/licenses/>.
"""
# SPFS Type Hints
#
# This file contains classes for use in type hints so that all types can be
# expressed in method docstrings.
#
# See:
# https://peps.python.org/pep-0589/
# https://adamj.eu/tech/2021/05/10/python-type-hints-how-to-use-typeddict/

import datetime
from typing import TypedDict


class RecoveredFile(TypedDict):
    """
    A ``TypedDict`` with two attributes:

    * ``name`` - the original name of the file, ``str``
    * ``data`` - the file contents, ``bytes``
    * ``mime`` - the MIME-type of the file, ``str``
    """
    name: str
    data: bytes
    mime: str


class FileInfo(TypedDict):
    """
    A ``TypedDict`` with four attributes:

    * ``name`` - the original name of the file, ``str``
    * ``size`` - the file size in the largest applicable units, ``str``
    * ``type`` - the MIME type of the file (empty if MIME type checking disabled), ``str``
    * ``time`` - the last modified time of the file, ``datetime.datetime``
    * ``url`` - the URL of the file, ``str``
    """
    name: str
    size: str
    type: str
    time: datetime.datetime
    url: str  # https://flask.palletsprojects.com/en/2.1.x/api/#flask.url_for
