"""
This file is part of Simple Python File Server (SPFS),
a simple application for uploading and downloading files
from old and new browsers.
Copyright (C) 2020-2023 Luna Lucadou

SPFS is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

SPFS is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SPFS.
If not, see <https://www.gnu.org/licenses/>.
"""
import re
import socket
from typing import List, Optional

from flask_babel import gettext

from app.utils import spfs_type_hints

MSIE3_USER_AGENT_BASE: str = 'MSIE 3.01'  # Derived from: Mozilla/3.0 (compatible; MSIE 3.01; Mac_68000)
# This regex was built using the following examples:
# --L0BYq3FKeh\r\nContent-disposition: attachment; filename="C:\\\\Documents and Settings\\\\Win2K\\\\Desktop\\\\test.txt"\r\nContent-type: application/octet-stream\r\nContent-transfer-encoding: binary\r\n\r\nthis is a test\r\nof udiwww upload\r\nability\r\n--L0BYq3FKeh--
# --5lWXACt1o8\r\nContent-disposition: attachment; filename="C:\\\\WINDOWS\\\\Desktop\\\\hello.txt"\r\nContent-type: application/octet-stream\r\nContent-transfer-encoding: binary\r\n\r\nhello world\r\n--5lWXACt1o8--
# It has 2 match groups:
#   1. Filename
#   2. File contents (note that this should be assumed to be hex)
#   3. File MIME type (supplied by the browser)
UDIWWW_FILE_PARSE_REGEX: str = \
    r"--\S+\s+Content-disposition:\s+attachment;\s+" \
    r"filename=\"(.+?)\"\s+" \
    r"Content-type:\s(.+)\s+" \
    r"Content-transfer-encoding:\sbinary\s{4}" \
    r"([\S\s]+)--\S+--"
UDIWWW_USER_AGENT_BASE: str = 'UdiWWW/1.2'  # Derived from: UdiWWW/1.2.000


def decode_buggy_file(file_data: str, user_agent: str) -> \
        Optional[spfs_type_hints.RecoveredFile]:
    """
    Given a file string sent by a browser that fails to properly implement the
    HTML 3 specification and its user agent, manually scrapes the file data
    and returns it in a RecoveredFile object.

    If no file is able to be recovered (either because it is not present or the
    browser is not yet supported), NoneType will be returned.

    :param file_data: the contents of the ``file`` field
    :param user_agent: the user-agent string given in the request
    :return: the filename, type, and contents, or None if no file was recovered
    """
    f_search_pattern: str = ''
    if UDIWWW_USER_AGENT_BASE in user_agent:
        f_search_pattern: str = UDIWWW_FILE_PARSE_REGEX
    f_search_res: Optional[re.Match] = re.search(f_search_pattern, file_data)
    rf: Optional[spfs_type_hints.RecoveredFile] = None
    if len(f_search_pattern) > 1 and f_search_res:  # No match = NoneType object
        rf: spfs_type_hints.RecoveredFile = {
            'name': str(f_search_res.group(1)),
            'data': f_search_res.group(3).encode(),
            'mime': str(f_search_res.group(2))
        }
    else:
        print(gettext('log.error.file.recovery'))
    return rf


def get_browser_warnings(user_agent: str) -> Optional[List[str]]:
    """
    Given a user-agent string, checks to see if there are any messages that
    need to be shown to the user on the upload page.

    Returns None if no warnings required.

    :param user_agent: the user-agent of the requester
    :return: any appropriate warnings or None
    """
    warnings: List[str] = []
    if UDIWWW_USER_AGENT_BASE in user_agent:
        warnings += [gettext('flash.warning.client.file.text-only')]
    if MSIE3_USER_AGENT_BASE in user_agent:
        warnings += [gettext('flash.error.client.ie3')]
    if len(warnings) > 0:
        return warnings
    else:
        return None


def get_local_ip() -> str:
    """
    Gets the IPv4 address of the interface with the default route.
    For use in configuring the default server if not set via config file.

    Does not consider IPv6 because this IP is only used when a browser is
    old enough that not having a hostname in URLs might cause problems, and
    anything written to support IPv6 is likely new enough that it won't use
    anything below HTTP/1.1, which is a requirement for these problems to
    occur (see issue #9).

    In addition, if the hostname is specified in the config file as a domain,
    the client can pick between the A and AAAA records, and the DNS records
    can contain non-publicly routable or publicly routable IP addresses,
    rather than relying on the default interface's (likely) private IPv4
    address.

    Adapted from https://stackoverflow.com/a/28950776/15264046

    :return: the IP address the server will (most likely) be receiving
    requests on
    """
    s: socket.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.settimeout(0)
    local_ip: str = ''

    try:
        # Does not need to be reachable
        s.connect(('10.255.255.255', 1))
        local_ip += s.getsockname()[0]
    except Exception:  # No interface with default route
        local_ip += '127.0.0.1'
    finally:
        s.close()

    return local_ip
